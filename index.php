<?php include('configuration/website.php'); ?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo SITE_TITLE ?></title>
        <meta name="robots" content="index,nofollow" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
        <meta content="utf-8" http-equiv="encoding">
        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
        <meta http-equiv="pragma" content="no-cache" />
        <style>a,abbr,acronym,address,applet,article,aside,audio,b,big,blockquote,body,canvas,caption,center,cite,code,dd,del,details,dfn,div,dl,dt,em,embed,fieldset,figcaption,figure,footer,form,h1,h2,h3,h4,h5,h6,header,hgroup,html,i,iframe,img,ins,kbd,label,legend,li,mark,menu,nav,object,ol,output,p,pre,q,ruby,s,samp,section,small,span,strike,strong,sub,summary,sup,table,tbody,td,tfoot,th,thead,time,tr,tt,u,ul,var,video{margin:0;padding:0;border:0;font:inherit;vertical-align:baseline}article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}body{line-height:1}ol,ul{list-style:none}blockquote,q{quotes:none}blockquote:after,blockquote:before,q:after,q:before{content:'';content:none}table{border-collapse:collapse;border-spacing:0}</style>
        <link rel="stylesheet" type="text/css" href="includes/css/style.min.css" />
        <link href="http://fonts.googleapis.com/css?family=Oswald:400,700" rel="stylesheet" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" defer></script>
        <script src="includes/js/global.min.js" defer></script>
        <link rel="author" href="<?php echo SITE_HOME ?>/humans.txt" type="text/plain" />
        <link rel="canonical" href="<?php echo SITE_HOME ?>" />
        <link rel="icon" type="image/png" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAgMAAABinRfyAAAACVBMVEUAAAAHo/8Opf70D4ByAAAAAXRSTlMAQObYZgAAAD5JREFUCNclzDENwEAMwED/kDIIhPIohAzxXlKVCvdfynKSFwMvEAVLH0KL1CY7G/gOy0NaoBBT9nDrPwMu2N9GDBLRUicEAAAAAElFTkSuQmCC" />
        <meta name="description" content="<?php echo SITE_DESCRIPTION ?>" />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:site" content="<?php echo SITE_TWITTER ?>" />
        <meta name="twitter:title" content="<?php echo SITE_TITLE ?>" />
        <meta name="twitter:description" content="<?php echo SITE_DESCRIPTION ?>" />
        <meta name="twitter:image" content="<?php echo SITE_HOME ?>/images/card.jpg" />
        <meta property="og:title" content="<?php echo SITE_TITLE ?>" />
        <meta property="og:site_name" content="<?php echo SITE_TITLE ?>" />
        <meta property="og:description" content="<?php echo SITE_DESCRIPTION ?>" />
        <meta property="og:url" content="<?php echo SITE_HOME ?>"/>
        <meta property="og:image" content="<?php echo SITE_HOME ?>/images/card.jpg" />
    </head>
    <body>
        <img id="logo" class="faded" src="<?php echo SITE_HOME ?>/images/payday2.png" width="400" height="63" alt="Payday 2 Logo" />
        <p id="subtitle" class="faded"><?php echo SITE_SUBTITLE ?></p>
        <div id="paint">
            <div id="container">
                <p id="mission_name">&nbsp;</p>
                <p id="mission_length" class="left details">&nbsp;</p>
                <p id="mission_mode" class="right details">&nbsp;</p>
                <p id="mission_contact" class="left details">&nbsp;</p>
                <p id="mission_dlc" class="right details">&nbsp;</p>
                <p id="link"><a href="#">Pick Another Mission</a></p>
                <p id="greenscreen" title="Toggle green screen">&#9728;</p>
            </div>
        </div>
        <p class="disclaimer">
            This page is not affiliated with <a href="http://www.overkillsoftware.com">Overkill Software</a> or <a href="http://http://starbreeze.com/">Starbreeze Studios</a>.
            Payday2 and all other <a href="http://www.overkillsoftware.com">Overkill Software</a> products names are trademarks or registered trademarks of Overkill Software.
            CrimeNet background provided by <a href="http://swedishcheese.deviantart.com/art/CrimeNet-Wallpaper-Payday-2-499062720" rel="nofollow">SwedishCheese</a> although it has been modified for web use.
            All other company and product names are trademarks or registered trademarks of their respective companies.
        </p>
        <p id="contact" class="disclaimer">
            <a href="https://bitbucket.org/tecoBIT/payday2-random-missions" rel="nofollow">Source code available on Bit Bucket</a>
        </p>
    </body>
</html>