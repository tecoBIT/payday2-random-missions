$(document).ready(function(){
//------------------------------------------------------------------------------
//Globals ----------------------------------------------------------------------
//------------------------------------------------------------------------------
    var bLocked = false;                                                        //Can't spam button more than 2x a second
    var jsonHeists;                                                             //Store the Heist Data here
    var oMissionName = $("#mission_name");                                      //Common html elements will be stored as variables
    var oMissionLength = $("#mission_length");                                  //  to avoid searching for the elements every single time
    var oMissionMode = $("#mission_mode");                                      //  we try to update the page. Find them one, cache it and move on.
    var oMissionContact = $("#mission_contact");
    var oMissionDLC = $("#mission_dlc");
    var iLastIndex = -1;                                                        //Track the last know mission, avoid showing same one twice

//------------------------------------------------------------------------------
//Functions --------------------------------------------------------------------
//------------------------------------------------------------------------------
    //[g]etHeists
    /*  Load the list of heists from the JSON configuration file and then store
     *  that data as a global variable.  We're doing it this way so that all the work
     *  is done client-side and has no ill-effect on the server-side beyond the initial load.
     *  @params     none;
     *  @returns    nothing
     */
    function getHeists() {
        $.getJSON('configuration/heists.json', function(data) {                 //Load the Heist JSON File
            jsonHeists = data.missions;                                         //Store the values in our global
            getRandomHeist();                                                   //Go back to getRandomHeists and process
        });
    }

    //[g]etMode
    /*  The "Mission Mode" is stored as an integer in the JSON array.  We're going
     *  to convert that into a human readable string for the UI.
     *  1 = Loud Mission && 2 = Stealth Only && 3 = Stealth or Loud
     *  @params     integer     required        the "Mission Mode"
     *  @returns    string                      The Mission mode in human readable format
     */
    function getMode(iMode) {
        switch (parseInt(iMode)) {
            case 1:     return "Loud Only"; break;
            case 2:     return "Stealth Only"; break;
            case 3:     return "Stealthable"; break;
            default:    return "Unknown ["+iMode+"]";
        }
    }

    //[g]etRandomIndex
    /*  Picks a random number that corresponds with our JSON array of available
     *  missions.  This is a separate function, instead of inline code so that
     *  we can experiment with different random algorithsm, simplify updates to
     *  the picker and keep track of the last picked mission to avoid picking the
     *  same mission twice.
     *  @params     none
     *  @returns    integer
     */
    function getRandomIndex() {
        iRand = iLastIndex;
        while(iLastIndex == iRand) {
            iRand = Math.floor(Math.random()*jsonHeists.length);
        }
        iLastIndex = iRand;
        return iRand;
    }

    //[g]etRandomHeist
    /*  This is the "meat and potatoes" of the entire script.  It handles locking
     *  the UI briefly to avoid animation interruption, checking to see if the
     *  Heists array needs to be loaded, randomly picks a heist and raws it on screen.
     *  @params     none
     *  @returns    nothing                     Draws the randomly chosen heist on screen
     */
    //  The "Meat and Potatoes" of the entire library.    Picks a new random heist
    //  when the link is clicked or the page is first loaded.
    function getRandomHeist() {
        if(bLocked) { return;}                                                  //You're allowed 1 render every 1/2 second
        if(typeof jsonHeists === 'undefined') {                                 //If the Heist list hasn't been repviously loaded,
            jsonHeists = getHeists();                                           //Then call the getHeists function to load that data
            return;                                                             //abort further processing because we need the list loaded
        }
        shuffleHeists();                                                        //Shuffle the heists array (redundantly random)
        iRand = getRandomIndex();                                               //Pick a random array index (Function to check for previous matches)

        //Lock the UI for 1/2 a second (avoid spam clicking)
        bLocked = true;                                                         //UI Locked, no more rendering missions
        setTimeout(unlock, 501);                                                //Unlock the UI in 501 milliseconds

        //Update Content (Animate Fade In/Out - 250Out, 250In: 500ms total)
        oMissionName.fadeOut(250,function(){oMissionName.html(jsonHeists[iRand].name);}).fadeIn(250);
        oMissionLength.fadeOut(250,function(){oMissionLength.html(jsonHeists[iRand].days + " Day mission");}).fadeIn(250);
        oMissionMode.fadeOut(250,function(){oMissionMode.html(getMode(jsonHeists[iRand].mode))}).fadeIn(250);
        oMissionContact.fadeOut(250,function(){oMissionContact.html("Contact: " + jsonHeists[iRand].contact);}).fadeIn(250);
        oMissionDLC.fadeOut(250,function(){oMissionDLC.html(jsonHeists[iRand].dlc);}).fadeIn(250);
    }

    //[s]huffleHeists
    /*  Shuffle the heists array before randomly selecting an array index.  Very
     *  much an overkill situation here eto randomize the order before randomly
     *  selecting an element, but browser impact is minimal, and ensures a more
     *  random experience.   Separate function so it can be toggled/adjusted
     *  to different shuffle algorithms if needed.
     *  @params     none
     *  @returns    nothing                     Shuffles the Global Heists Array
     */
    function shuffleHeists() {
        jsonHeists.sort(function(){ return 0.5 - Math.random();});
    }

    //[t]oggleGreenScreen
    /*  Strips the header & background content to only leave the "paintbrush" mission
     *  selection window.  The background is then converted to a green screen that
     *  can be used for streamers to Chromakey the background out for their broadcasts
     *  @params     none
     *  @returns    nothing
     */
    function toggleGreenScreen() {
        $("#logo").toggle();
        $("#subtitle").toggle();
        $(".disclaimer").toggle();
        $("body").toggleClass("greenscreen");
    }

    //[u]nlock
    /*  Unlocks the UI.   This is primarily called from the getRandomHeist() function
     *  on a delayed timer.  This ensures animations can complete before the user can
     *  interact with the screen again.
     *  @params     none
     *  @returns    nothing                     Adjusts a global variable
     */
    function unlock() {
        bLocked = false;
    }

//------------------------------------------------------------------------------
//Routines ---------------------------------------------------------------------
//------------------------------------------------------------------------------
    //"Pick Another Mission" Clicked
    $("#link").click(function() {
        getRandomHeist();
    });

    //Greenscreen Toggle Clicked
    $("#greenscreen").click(function() {
        toggleGreenScreen();
    });

//------------------------------------------------------------------------------
//Startup ----------------------------------------------------------------------
//------------------------------------------------------------------------------
    getRandomHeist();
});