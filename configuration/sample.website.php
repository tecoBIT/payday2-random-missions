<?php
/*  WEBSITE CONFIGURATION
 *      Use this file to configure some meta data for your self-hosted version
 *      of this code.   Once completed, save this file AS   "website.php"
 *
 *      Configuration Values
 *          SITE_HOME               This should be the absolute url to your website
 *                                  and should NOT contain the trailing slash.
 *
 *          SITE_TITLE              This will be used as the document's Title as well
 *                                  as meta data from Twitter and OpenGraph (Facebook)
 *
 *          SITE_SUBTITLE           This is displayed in the UI under the Payday2 logo
 *
 *          SITE_DESCRIPTION        Used in Meta Tags for Search Engines, Twitter and OpenGraph
 *
 *          SITE_TWITTER            Required for the Twitter Card Meta Tags.
 */
    define("SITE_HOME",                 "http://YourWebsite.com");
    define("SITE_TITLE",                "Pick a Random Payday Mission");
    define("SITE_SUBTITLE",             "CrimeNET Random Mission");
    define("SITE_DESCRIPTION",          "Tired of picking missions? Hand your fate over to the internet");
    define("SITE_TWITTER",              "@yourTwitterID");
?>