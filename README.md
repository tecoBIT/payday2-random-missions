#Payday2 Random Mission Selector#

A common problem in Payday 2 is deciding what contract your group is going to run.
Rather than waste time debating or recycling the same old repetitive maps, let us
pick a random mission for you.

Once initialized, the server will respond with assets and libraries.  After that
initial load, all subsequent randomization and mission selection is handled client-side to
alleviate strain on the servers.

Additionally a "green screen" option has been added to allow streamers to chroma-key
the interface into their broadcasts.   The "sun/light" icon on the right will toggle the
green screen display.  [RGB: 0,255,36 || HEX: 00ff24 || CMYK: 60,0,100,0]  (See [Screenshots](https://bitbucket.org/tecoBIT/payday2-random-missions/src/master/screenshots/))

##REQUIREMENTS##
###Server-Side###
  * PHP 5.3+
  * Apache (mod\_rewrite, mod\_deflate, mod\_gzip)

###Client-Side###
  * Modern Browser (CSS3 + jQuery Compatible)
  * Internet Connection

##CONFIGURATION##
###Website###
Within the "configuration" folder there is a file named "**[sample.website.php](https://bitbucket.org/tecoBIT/payday2-random-missions/src/master/configuration/sample.website.php)**".
Edit the values as needed and save this file as "**website.php**"
Configuration settings are as follows

 * **SITE_HOME**: Fully qualified URL to your root folder
 * **SITE_TITLE**: Used in the page's title tag as well as Twitter and Facebook meta tags
 * **SITE_SUBTITLE**: Displayed in the main UI under the Payday 2 logo
 * **SITE_DESCRIPTION**: Used in Meta Tags
 * **SITE_TWITTER**: Used in Twitter card meta tag

###Payday Heist List###
There is a JSON file titled "**[heists.json](https://bitbucket.org/tecoBIT/payday2-random-missions/src/master/configuration/heists.json)**" within the configuration folder.  This is
a matrix of available missions and their configurations.  You can add or remove
as many missions as needed as long as you stick to maintaining the proper format and
key / value settings.

  * **contact**: String of the contract broker. (e.g.: "Bain")
  * **dlc**: String of the required DLC Pack.  Null of part of base game. (e.g.: "Transport DLC")
  * **mode**: Integer that represents the available mission modes. (1=Loud-Only, 2=Stealth-Only, 3=Stealth Optional)
  * **days**: Integer that represents the number of days in the contract.
  * **name**: String of the mission's name (e.g.: "Hoxton Breakout Pro")


##Disclaimer##
This project is not affiliated with Overkill Software or Starbreeze Studios. Payday2 and all other Overkill Software products names are trademarks or registered trademarks of Overkill Software.
All other company and product names are trademarks or registered trademarks of their respective companies.
See _humans.txt_ for all additional attributions.

##License##
  * Payday2 Logo & Background: (C)Overkill Software
  * All other assets released under the [MIT License](http://opensource.org/licenses/MIT)